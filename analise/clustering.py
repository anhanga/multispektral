import matplotlib.pyplot as plt 
from PIL import Image
import numpy as np
import matplotlib.image as img 
 
imgR  = Image.open('r-photo.jpg')
imgG  = Image.open('g-photo.jpg')
imgB  = Image.open('b-photo.jpg')
imgIR = Image.open('ir-photo.jpg')

width, height = imgR.size 

imgIRR,imgIRG,imgIRB = imgIR.split()
imgRR,imgRG,imgRB = imgR.split()
imgGR,imgGG,imgGB = imgG.split()
imgBR,imgBG,imgBB = imgB.split()

R = np.array(imgRR)
G = np.array(imgGG)
B = np.array(imgBB)
IR = np.array(imgIRG)

IR = IR.reshape(1,(width*height))
RV = R.reshape(1,(width*height))
GV = G.reshape(1,(width*height))
BV = B.reshape(1,(width*height)) 
    
imgIRGB = np.zeros((height,width,4))

imgIRGB[:,:,0] = R /max(max(RV))
imgIRGB[:,:,1] = G /max(max(GV))
imgIRGB[:,:,2] = B /max(max(BV))
imgIRGB[:,:,3] = IR /max(max(IR))

plt.subplot(2,3,1)
plt.imshow(imgR)
plt.subplot(2,3,2)
plt.imshow(imgG)
plt.subplot(2,3,3)
plt.imshow(imgB)
plt.subplot(2,3,4)
plt.imshow(imgIR)
plt.subplot(2,3,5)
plt.imshow(imgIRGB)
plt.show()

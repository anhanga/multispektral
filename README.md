# Multispektral
Multispektral is a simple system designed to run with an ESP32 to take multispectral images.

This project aims to be able to adquire multispectral images over a simple camera using a PCB
(printed circuit board) with some differents LEDs of different colors for sample ilumination.
The LEDs are controlled by a ESP32 microcontroller, that take a picture for each LED color
and sends to a computer. I also put here the PCB files from Kicad (software wich I
made it). 
